Introduction
============
This product enables users to add a static text portlet that will randomly select pages from a collection.

The typical use case revolves around a "highlight" portlet where there are any number of pages that could be the featured hightlight. Installing this product allows you to assign this portlet to some section of the site and point it to a collection. By default, it will randomly pull one of those pages from your collection for display.

It is designed to be used with static text portlets as it will display the body content of that object (as opposed to the Title and Description one would typically get with a standard collection portlet). While this would work with a collection of, say, events (or any other content type), it probably wouldn't be useful as it would display the body text of the event, rather than the useful metadata those most users would expect.