from zope.i18nmessageid import MessageFactory
RandomStaticPortletMessageFactory = MessageFactory('eberly.portlet.RandomTextPortlet')


def initialize(context):
    """Initializer called when used as a Zope 2 product."""
