from setuptools import setup, find_packages
import os

version = '1.0'

setup(name='eberly.RandomTextPortlet',
      version=version,
      description="",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
        ],
      keywords='',
      author='Joe DeLuca',
      author_email='jad52@psu.edu',
      url='http://science.psu.edu',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['eberly', ],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-

      [z3c.autoinclude.plugin]
      target = plone
      """,
      )
